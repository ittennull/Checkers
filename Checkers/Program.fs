﻿module Main

open System.IO
open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.RequestErrors
open Suave.Writers
open AI
open Game
open Models
open System.Net

let rootDir = Path.GetFullPath "."
let webDir = Path.Combine(rootDir, "web")
let indexHtmlPath = Files.resolvePath webDir "index.html"

let ui = 
    choose [
        GET >=> path "/" >=> Files.file indexHtmlPath //server redirect from "/" to "/web/index.html"
        GET >=> Files.browse webDir
        NOT_FOUND "File not found" 
    ]


module ApiModule =
    let boardField = "board"
    let pieceIdField = "pieceId"
    let onlyJumpField = "onlyJump"

    type PossiblePositionsRequest = {
        board: Board
        pieceId: int
        onlyJump: bool
    }

    type MoveRequest = {
        board: Board
        pieceId: int
        position: Position
    }

    let newGame = newGame |> Serialization.toJson |> OK
    
    let movePiece (data: MoveRequest) =
        let piece = getPieceById data.board data.pieceId
        let newBoard = playerMovesPiece data.board piece data.position
        newBoard |> Serialization.toJson |> OK

    let aiMove (board: Board) =
        move board |> Serialization.toJson |> OK

    let getPossiblePositions (data: PossiblePositionsRequest) = 
        let piece = getPieceById data.board data.pieceId
        let positions = getPossiblePositions data.board data.onlyJump piece 
        positions |> Serialization.toJson |> OK

    let parseAndProcess<'a> (f: 'a -> WebPart) =
        request (fun req -> Serialization.getRequestData<'a> req.rawForm |> f)
        
    

let apiPrefix = "/api/"
let apiPath path = Suave.Filters.path (apiPrefix + path)
let api =
    pathStarts apiPrefix >=> choose [
        GET >=> apiPath "new" >=> ApiModule.newGame
        POST >=> apiPath "move" >=> ApiModule.parseAndProcess ApiModule.movePiece
        POST >=> apiPath "aimove" >=> ApiModule.parseAndProcess ApiModule.aiMove
        POST >=> apiPath "possible_positions" >=> ApiModule.parseAndProcess ApiModule.getPossiblePositions
    ] >=> setMimeType "application/json; charset=utf-8"
    

let app = choose [api; ui]

let config = { defaultConfig with 
                homeFolder = Some rootDir
                bindings = 
                [
                    HttpBinding.create HTTP IPAddress.Any HttpBinding.DefaultBindingPort
                ] }


startWebServer config app