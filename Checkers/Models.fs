﻿module Models

type Team = White | Black


type Position = {
    X: int
    Y: int
}

type Piece = {
    Id: int
    Position: Position
    Crowned: bool
    Team : Team
}


type Board = {
    Pieces: Piece list
}

type MoveResults = {
    Board: Board
    PieceId: int
    CrownedPosition: Position option
    EatenPieceIds: int list
    Positions: Position list
    WonTeam: Team option
}

type PlayerMoveResult = {
    moveResults: MoveResults
    canContinue: bool
}
