﻿module Serialization

open Newtonsoft.Json
open Newtonsoft.Json.Serialization
open System.Text


let private settings = new JsonSerializerSettings()
settings.ContractResolver <- new CamelCasePropertyNamesContractResolver() 

let toJson model = 
    JsonConvert.SerializeObject(model, settings)

let getRequestData<'a> (bytes: byte array) =
    let str = Encoding.UTF8.GetString(bytes)
    JsonConvert.DeserializeObject<'a>(str)