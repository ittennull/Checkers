﻿module AI
open Models
open Game


let private emptyMoveResults board =
    {   Board = board
        PieceId = -1
        EatenPieceIds = []
        CrownedPosition = None
        Positions = []
        WonTeam = None }

let private initialMoveResults board pieceId = 
    let piece = getPieceById board pieceId
    {emptyMoveResults board with PieceId = pieceId; Positions = [piece.Position]}

let private addMoveResults result board crowned eatenPieceIds pos =
    {   Board = board
        PieceId = result.PieceId
        CrownedPosition = 
            if result.CrownedPosition.IsSome then result.CrownedPosition 
            elif crowned then Some pos
            else None
        EatenPieceIds = result.EatenPieceIds @ eatenPieceIds
        Positions = result.Positions @ [pos]
        WonTeam = None }


let rec private movePiece board onlyJump initialResults piece = 
    let processMove =
        function
        | Normal (pos, _) -> 
            let newBoard = movePieceOnBoard board piece pos
            let movedPiece = getPieceById newBoard piece.Id
            let crowned = not(piece.Crowned) && movedPiece.Crowned
            let moveResults = addMoveResults initialResults newBoard crowned [] movedPiece.Position
            [moveResults]
        | Jump (pos, moveDirection) ->
            let eatenPiecePosition = getPositionFrom piece.Position moveDirection |> Option.get
            let eatenPiece = getPiece board eatenPiecePosition |> Option.get
            let newBoard = movePieceOnBoard board piece pos
            let newBoard = removePiece newBoard eatenPiece
            let movedPiece = getPieceById newBoard piece.Id
            let crowned = not(piece.Crowned) && movedPiece.Crowned
            let moveResults = addMoveResults initialResults newBoard crowned [eatenPiece.Id] pos
            moveResults :: movePiece newBoard true moveResults movedPiece 

    getPossibleMoveDirections onlyJump piece
        |> List.map (fun moveDirection -> movePieceToDirection board piece moveDirection onlyJump)
        |> List.choose id
        |> List.collect processMove


let calculateMoveResultsForAllTeamPieces board team = 
    board.Pieces
    |> List.filter (fun x -> x.Team = team)
    |> List.map (fun piece ->
        let initialResults = initialMoveResults board piece.Id
        movePiece board false initialResults piece)
    |> List.collect id
    |> List.map (fun moveResults -> {moveResults with WonTeam = whoWon moveResults.Board})

let getBestMoveResults board team =
    calculateMoveResultsForAllTeamPieces board team
    |> List.sortWith (fun x y -> 
        if x = y then 0
        elif x.EatenPieceIds.Length > y.EatenPieceIds.Length then -1
        elif x.CrownedPosition.IsSome then -1
        else 1 )
    |> List.truncate 5

let rateMoveResults moveResults =
    let winScore = 1000
    let eatFactor = 10
    let crownFactor = 2
    
    if moveResults.WonTeam.IsSome then winScore
    else moveResults.EatenPieceIds.Length * eatFactor + (Option.count moveResults.CrownedPosition) * crownFactor

let move board =
    let bestMoveResultOption =
        calculateMoveResultsForAllTeamPieces board Black
            |> List.map (fun blackMoveResults ->
                getBestMoveResults blackMoveResults.Board White
                |> List.map rateMoveResults
                |> List.sortDescending
                |> List.tryHead
                |> Option.orDefault -9999
                |> fun whiteRating -> 
                    let blackRating = rateMoveResults blackMoveResults
                    blackMoveResults, blackRating - whiteRating
                )
            |> List.sortByDescending snd
            |> List.tryHead
            |> Option.map fst
            
    match bestMoveResultOption with
    | Some moveResults -> moveResults
    | None -> {emptyMoveResults board with WonTeam = whoWon board}
    
