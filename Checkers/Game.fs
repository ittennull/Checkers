﻿module Game
open Models


let private piece id team position = {
    Id = id
    Position = {X = fst position; Y = snd position}
    Crowned = false
    Team = team
}


let newGame = {
    Pieces = 
    [
        (Black, (1, 0))
        (Black, (3, 0))
        (Black, (5, 0))
        (Black, (7, 0))
        
        (Black, (0, 1))
        (Black, (2, 1))
        (Black, (4, 1))
        (Black, (6, 1))
        
        (Black, (1, 2))
        (Black, (3, 2))
        (Black, (5, 2))
        (Black, (7, 2))


        (White, (0, 5))
        (White, (2, 5))
        (White, (4, 5))
        (White, (6, 5))
        
        (White, (1, 6))
        (White, (3, 6))
        (White, (5, 6))
        (White, (7, 6))
        
        (White, (0, 7))
        (White, (2, 7))
        (White, (4, 7))
        (White, (6, 7))
    ]
    |> List.mapi (fun index (team, position) -> piece index team position)
}


type MoveDirection = LeftUp | RightUp | LeftDown | RightDown

type Move = 
    | Jump of Position*MoveDirection
    | Normal of Position*MoveDirection


let getPositionFrom {X=x; Y=y} moveDirection =
    match moveDirection with
    | LeftUp when x > 0 && y > 0 -> Some {X = x - 1; Y = y - 1}
    | RightUp when x < 7 && y > 0 -> Some {X = x + 1; Y = y - 1}
    | LeftDown when x > 0 && y < 7 -> Some {X = x - 1; Y = y + 1}
    | RightDown when x < 7 && y < 7 -> Some {X = x + 1; Y = y + 1}
    | _ -> None

let getJumpPositionFrom {X=x; Y=y} moveDirection = 
    match moveDirection with
    | LeftUp when x > 1 && y > 1 -> Some {X = x - 2; Y = y - 2}
    | RightUp when x < 6 && y > 1 -> Some {X = x + 2; Y = y - 2}
    | LeftDown when x > 1 && y < 6 -> Some {X = x - 2; Y = y + 2}
    | RightDown when x < 6 && y < 6 -> Some {X = x + 2; Y = y + 2}
    | _ -> None

let private moveDirectionsForWhites = [LeftUp; RightUp]
let private moveDirectionsForBlacks = [LeftDown; RightDown]

let getPossibleMoveDirections onlyJump (piece: Piece) =
    match (piece.Crowned || onlyJump), piece.Team with
    | (true, _) -> moveDirectionsForWhites @ moveDirectionsForBlacks
    | (_, Black) -> moveDirectionsForBlacks
    | (_, White) -> moveDirectionsForWhites

let getPiece board position = board.Pieces |> List.tryFind (fun x -> x.Position = position)
let getPieceById board pieceId = board.Pieces |> List.find (fun x -> x.Id = pieceId)
    
let isOccupied board position = getPiece board position |> Option.isSome

let movePieceToDirection board piece moveDirection onlyJump =
    let isValidJumpOverOtherTeamPiece pos1 =
        let otherPiece = getPiece board pos1
        match otherPiece with
        | Some p -> p.Team <> piece.Team
        | _ -> false

    let oneStepPosition = getPositionFrom piece.Position moveDirection
    let jumpPosition = getJumpPositionFrom piece.Position moveDirection

    match onlyJump, oneStepPosition, jumpPosition with
    | (false, Some pos, _) when isOccupied board pos |> not -> Some (Normal (pos,moveDirection))
    | (_, Some pos1, Some pos2) when isOccupied board pos2 |> not && isValidJumpOverOtherTeamPiece pos1 -> Some (Jump (pos2,moveDirection))
    | _ -> None


let getPossiblePositions board onlyJump piece = 
    getPossibleMoveDirections onlyJump piece
        |> List.map (fun moveDirection -> movePieceToDirection board piece moveDirection onlyJump)
        |> List.choose id
        |> List.map (function Jump (pos, _) | Normal (pos, _) -> pos )


let movePieceOnBoard board piece pos =
    let doesGetCrown team {Y=y} =
        match team with
        | Black -> y = 7
        | White -> y = 0

    let otherPieces = board.Pieces |> List.except [piece]
    let crowned = piece.Crowned || doesGetCrown piece.Team pos
    let pieces = {piece with Position = pos; Crowned = crowned} :: otherPieces
    {Pieces = pieces}


let removePiece board piece = {Pieces = board.Pieces |> List.except [piece] }


let whoWon board =
    let isGameOverForTeam team=
        board.Pieces
            |> List.filter (fun x -> x.Team = team)
            |> List.forall (fun x -> getPossiblePositions board false x = [] )

    if isGameOverForTeam White then Some Black
    elif isGameOverForTeam Black then Some White
    else None


let playerMovesPiece board piece position =
    let isJump = System.Math.Abs(piece.Position.X - position.X) = 2
    let newBoard = movePieceOnBoard board piece position
    let movedPiece = getPieceById newBoard piece.Id
    let moveResults = 
        {   Board = newBoard
            PieceId = movedPiece.Id
            CrownedPosition = if not(piece.Crowned) && movedPiece.Crowned then Some position else None
            EatenPieceIds = []
            Positions = [piece.Position; movedPiece.Position]
            WonTeam = None }

    let moveResults = 
        match isJump with
        | true ->
            let positionDiff = {X = position.X - piece.Position.X; Y = position.Y - piece.Position.Y}
            let otherPiecePosition = {X = piece.Position.X + positionDiff.X / 2; Y = piece.Position.Y + positionDiff.Y / 2}
            let otherPiece = getPiece newBoard otherPiecePosition |> Option.get
            let newBoard = removePiece newBoard otherPiece
            {moveResults with EatenPieceIds = [otherPiece.Id]; Board = newBoard}
        | false -> moveResults

    let moveResults = {moveResults with WonTeam = whoWon moveResults.Board}
    let newPiece = getPieceById moveResults.Board moveResults.PieceId
    let canContinue = moveResults.WonTeam.IsNone && isJump && getPossiblePositions moveResults.Board true newPiece <> []
    {   moveResults = moveResults
        canContinue = canContinue}

    
    
