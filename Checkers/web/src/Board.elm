module Board exposing (
    createNewBoard,
    transformPositionToCoordinates
    )

import Types exposing (..)
import Dto exposing (..)
import BoardView exposing (..)

createNewBoard: BoardDto -> Maybe Int -> Board
createNewBoard boardDto selectedPieceId = 
    let createPiece pieceDto = {
                pieceDto = pieceDto,
                coordinates = transformPositionToCoordinates pieceDto.position,
                opacity = 1
            }
        pieces = List.map createPiece boardDto.pieces
    in {
           pieces = pieces,
           selectedPieceId = selectedPieceId,
           highlightedPlaces = []
       }

transformPositionToCoordinates position =
    let rectSize = toFloat BoardView.rectSize
        calc property = toFloat (property position) * rectSize + rectSize / 2
    in  { x = calc .row, y = calc .column }