module Model exposing(..)

import Types exposing (..)
import Msg exposing (Msg)

type alias Model = {
    board: Board,
    moveAnimations: List MoveAnimation,
    pieceDisappearAnimations: List OpacityAnimation,
    animationFinishAction: Maybe AnimationFinishAction,
    gameState: GameState,
    playerHasNotFinishedMove: Bool
    }

type AnimationFinishAction = AnimationFinishAction ((Model, Cmd Msg) -> (Model, Cmd Msg))