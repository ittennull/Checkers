module Main exposing (..)

import Html
import Msg exposing (..)
import Debug
import Model exposing (..)
import Types exposing (..)
import View exposing (..)
import AnimationFrame
import Ports exposing (..)
import AnimationHandler exposing (..)
import GameManagementMsg exposing (..)
import GameManagement exposing (..)
import GameControlMsg exposing (..)
import GameControl exposing (..)


main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }



init : (Model, Cmd Msg)
init =
    let model = {
            board = {
                pieces = [],
                selectedPieceId = Nothing,
                highlightedPlaces = []
            },
            moveAnimations = [],
            pieceDisappearAnimations = [],
            animationFinishAction = Nothing,
            gameState = Types.InProgress,
            playerHasNotFinishedMove = False
        }
    in (model, Cmd.none)


-- UPDATE

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GameManagement msg -> gameManagementUpdate msg model
        GameControl msg -> gameControlUpdate msg model
        AnimationFrameTick time -> onAnimationFrameTick time model


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    let animationSub = 
            if isAnimationInProgress model then
                [AnimationFrame.diffs AnimationFrameTick]
            else
                []
        
        gameLoadedSub = [gameLoaded (GameLoaded >> GameManagement)]

        subscribtions = animationSub ++ gameLoadedSub
    in  Sub.batch subscribtions
