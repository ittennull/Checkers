module Utils exposing (
    logHttpErrorOrProceed
    )


logHttpErrorOrProceed model response continuation =
    case response of
        Ok payload -> continuation model payload
        Err error -> Debug.log (toString error) (model, Cmd.none)