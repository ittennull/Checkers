port module Ports exposing(
    saveGame, 
    loadGame,
    gameLoaded
    )

import Json.Encode as Encode exposing(..)

port saveGame: Encode.Value -> Cmd msg
port loadGame: () -> Cmd msg

port gameLoaded: (Encode.Value -> msg) -> Sub msg