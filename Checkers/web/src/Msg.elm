module Msg exposing(
    Msg (..)
    )

import Time exposing (Time)
import GameManagementMsg exposing (..)
import GameControlMsg exposing (..)

type Msg 
    = GameManagement GameManagementMsg
    | GameControl GameControlMsg
    | AnimationFrameTick Time