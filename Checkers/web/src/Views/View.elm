module View exposing(
    view
    )

import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (..)
import BoardView exposing (..)
import Types exposing (..)
import Model exposing (..)
import GameManagementMsg exposing (..)
import GameControlMsg exposing (..)


menu model =
    let saveDisabledAttr = disabled (model.gameState /= InProgress)
    in  div [class "menu"] [
            button [onClick (GameManagement NewGameApiRequest)] [text "New game"],
            button [onClick (GameManagement LoadGame)] [text "Load game"],
            button [onClick (GameManagement SaveGame), saveDisabledAttr] [text "Save game"]
        ]

controls model =
    let activeClass = if model.playerHasNotFinishedMove then "active" else ""
    in  div [class "controls"] [
            button [id "finishMove", class activeClass, onClick (GameControl FinishMove)] [
                text "Finish move"
            ]
        ]

winOrLost model =
    let content = 
        case model.gameState of
            Won -> 
                div [class "game-result win"] [
                    text "You won!"
                ]
            Lost ->
                div [class "game-result lost"] [
                    text "You lost!"
                ]
            InProgress -> emptyNode
    in  div [] [content]

emptyNode = Html.text ""

view model =
  div [] [
        menu model,
        boardView model,
        controls model,
        winOrLost model
    ]
