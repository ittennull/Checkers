module BoardView exposing (
    boardView,
    rectSize
    )

import Html
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Html.Events exposing (..)
import Dto exposing (..)
import Msg exposing (..)
import Model exposing (..)
import Types exposing (..)
import GameControlMsg exposing (..)


rectSize = 100
numberOfPlaces = 8
boardSize = rectSize * numberOfPlaces

getRect isHighlighted {row, column} =
    let
        xCoord = row * rectSize
        yCoord = column * rectSize
        isWhite = (row + column) % 2 == 0
        className = if isWhite then "white" else "black"
        onClickAttr = if isHighlighted then [onClick (GameControl (PlaceClick (Position row column)))] else []
        highlightedClass = if isHighlighted then "highlighted" else ""
        positionalAttributes = [x (toString xCoord), y (toString yCoord), width (toString rectSize), height (toString rectSize)]
        attributes = [class ("board-place-container " ++ highlightedClass)] ++ onClickAttr
    in
        g attributes [
            rect ([class ("board-place " ++ className)] ++ positionalAttributes) [],
            rect ([class "move-highlighting", fill "url(#rgrad)"] ++ positionalAttributes) []
        ]

getRects highlightedPlaces = 
    let 
        numbers = List.range 0 (numberOfPlaces-1)
    in
        numbers 
        |> List.map (\row -> 
            numbers 
            |> List.map (\column -> Position row column)
            |> List.map (\pos -> 
                let isHighlighted = List.member pos highlightedPlaces
                in  getRect isHighlighted pos )
            )
        |> List.concat


getPiece isSelected playerHasNotFinishedMove piece =
    let
        circleClass = if piece.pieceDto.team == White then "white" else "black"
        selectedClass = if isSelected == True then " selected" else ""
        crownedClass = if piece.pieceDto.crowned then " crowned" else ""
        inActiveClass = if piece.pieceDto.team == White && playerHasNotFinishedMove then " inactive" else ""
        crownSize = rectSize / 2
        crownSizeStr = crownSize |> toString
        crownXY = -crownSize / 2 |> toString
        onClickAttr = 
            if piece.pieceDto.team == White && not playerHasNotFinishedMove then 
                [onClick (GameControl (PieceClick piece))] 
            else 
                []
        attributes = onClickAttr ++ [
            class ("piece-container " ++ selectedClass ++ crownedClass), 
            transform ("translate(" ++ (toString piece.coordinates.x) ++ "," ++ (toString piece.coordinates.y) ++ ")"),
            opacity (toString piece.opacity) ]
    in  g attributes [
            circle [class ("piece " ++ circleClass ++ inActiveClass), cx "0", cy "0", r "40"] [],

            svg [x crownXY, y crownXY, width crownSizeStr, height crownSizeStr, viewBox "0 0 236.893 236.893", Svg.Attributes.style "enable-background:new 0 0 236.893 236.893;"] [
                g [] [
                    Svg.path [fill "#FFFFFF", d "M221.31,75.235c-8.573,0-15.551,6.984-15.551,15.564c0,3.361,1.102,6.46,2.929,9.003l-50.017,31.746l-34.574-52.017   c5.785-2.268,9.901-7.873,9.901-14.452c0-8.58-6.979-15.558-15.558-15.558s-15.558,6.978-15.558,15.558   c0,6.591,4.123,12.184,9.91,14.452l-34.918,52.534L28.178,99.823c1.827-2.551,2.938-5.65,2.938-9.024   c0-8.58-6.978-15.564-15.558-15.564S0,82.219,0,90.799s6.979,15.558,15.558,15.558c1.641,0,3.172-0.326,4.652-0.792l37.984,60.282   c0.861,1.358,2.35,2.192,3.967,2.192h112.574c1.607,0,3.093-0.834,3.958-2.192l37.983-60.282c1.486,0.466,3.021,0.792,4.652,0.792   c8.586,0,15.564-6.979,15.564-15.558S229.896,75.235,221.31,75.235z M118.434,58.868c3.416,0,6.211,2.789,6.211,6.211   c0,3.431-2.789,6.205-6.211,6.205s-6.205-2.774-6.205-6.205C112.223,61.663,115.012,58.868,118.434,58.868z M9.341,90.799   c0-3.422,2.783-6.211,6.205-6.211c3.422,0,6.205,2.789,6.205,6.211c0,3.431-2.783,6.205-6.205,6.205   C12.124,97.003,9.341,94.23,9.341,90.799z M172.128,158.687H64.722l-25.581-40.59l37.503,24.333   c2.159,1.395,5.033,0.797,6.442-1.34l35.336-53.158l34.964,52.61c1.419,2.118,4.256,2.733,6.405,1.364l38.167-24.227   L172.128,158.687z M221.31,97.01c-3.422,0-6.198-2.773-6.198-6.205c0-3.422,2.776-6.211,6.198-6.211   c3.423,0,6.211,2.789,6.211,6.211C227.521,94.23,224.733,97.01,221.31,97.01z M179.783,182.697c0,2.575-2.095,4.676-4.677,4.676   H61.756c-2.582,0-4.676-2.101-4.676-4.676c0-2.582,2.094-4.677,4.676-4.677h113.35C177.688,178.02,179.783,180.115,179.783,182.697   z"][]
                ]
            ]
        ]

getPieces: Board -> Bool -> List (Svg Msg)
getPieces board playerHasNotFinishedMove =
    let isSelected piece =
            board.selectedPieceId
            |> Maybe.map ((==) piece.pieceDto.id) 
            |> Maybe.withDefault False
        getPiece2 piece = getPiece (isSelected piece) playerHasNotFinishedMove piece
    in  board.pieces |> List.map getPiece2

boardView: Model -> Html.Html Msg
boardView model = 
    let boardSizeVal = toString boardSize
    in  svg [width boardSizeVal, height boardSizeVal] ([
                defs [] [
                    radialGradient [id "rgrad", cx "50%", cy "50%", r "75%"] [
                        stop [offset "0%", Svg.Attributes.style "stop-color:rgb(87,255,241);stop-opacity:0.8"] [],
                        stop [offset "100%", Svg.Attributes.style "stop-color:rgb(0,128,128);stop-opacity:0.1"] []
                    ]
                ]
            ]
            ++ getRects model.board.highlightedPlaces
            ++ getPieces model.board model.playerHasNotFinishedMove
            )
