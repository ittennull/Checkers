module ApiClient exposing (
    startNewGame,
    getPossiblePositions,
    movePiece,
    aiMovePiece
    )

import Http
import Msg exposing (..)
import Decoding exposing (..)
import Encoding exposing (..)
import Json.Encode exposing (..)
import GameManagementMsg exposing (..)
import GameControlMsg exposing (..)

startNewGame =
    let url = "/api/new"
        handler response = GameManagement (NewGameApiResponse response)
    in Http.send handler (Http.get url decodeBoard)

getPossiblePositions board pieceId onlyJump =
    let url = "/api/possible_positions"
        body = encodeGetPossiblePositionsRequest board pieceId onlyJump 
               |> Http.jsonBody
        handler response = GameControl (GetPossiblePositionsResponse response)
    in Http.send handler (Http.post url body decodePositionList)

movePiece board pieceId position =
    let url = "/api/move"
        body = encodeMovePieceRequest board pieceId position
               |> Http.jsonBody
        handler response = GameControl (MovePieceResponse response)
    in Http.send handler (Http.post url body decodePlaerMoveResults)

aiMovePiece board =
    let url = "/api/aimove"
        body = encodeBoard board
               |> Http.jsonBody
        handler response = GameControl (AIMovePieceResponse response)
    in Http.send handler (Http.post url body decodeMoveResults)