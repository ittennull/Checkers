module Decoding exposing (
    decodeBoard,
    decodeMoveResults,
    decodePlaerMoveResults,
    decodePositionList
    )

import Json.Decode as Decode exposing(field, list, int, bool, string, nullable, index)
import Dto exposing (..)


decodeBoard : Decode.Decoder BoardDto
decodeBoard =
    let piecesDecoder = field "pieces" (list decodePiece)
    in Decode.map BoardDto piecesDecoder

decodePiece : Decode.Decoder PieceDto
decodePiece =
    Decode.map4 PieceDto 
        (field "id" int)
        (field "position" decodePosition)
        (field "crowned" bool)
        (field "team" decodeTeam)

decodePosition =
    Decode.map2 Position 
        (field "x" int)
        (field "y" int)

decodeTeam =
    let convertToTeam value = if value == "Black" then Black else White
    in Decode.map convertToTeam (decodeDU string)

decodeMoveResults =
    Decode.map6 MoveResults
        (field "board" decodeBoard)
        (field "pieceId" int)
        (field "crownedPosition" (nullable (decodeOption decodePosition)))
        (field "eatenPieceIds" (list int))
        (field "positions" (list decodePosition))
        (field "wonTeam" (nullable (decodeOption decodeTeam)))

decodePlaerMoveResults =
    Decode.map2 PlayerMoveResult 
        (field "moveResults" decodeMoveResults)
        (field "canContinue" bool)

decodePositionList = list decodePosition

decodeDU valueDecoder =
    field "case" valueDecoder

decodeOption valueDecoder =
    field "fields" (index 0 valueDecoder)