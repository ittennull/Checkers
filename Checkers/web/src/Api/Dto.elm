module Dto exposing (..)

type alias Position = {
    row: Int,
    column: Int
}

type Team = White | Black

type alias PieceDto = {
    id: Int,
    position: Position,
    crowned: Bool,
    team: Team
}

type alias BoardDto = {
    pieces: List PieceDto
}

type alias MoveResults = {
    board: BoardDto,
    pieceId: Int,
    crownedPosition: Maybe Position,
    eatenPieceIds: List Int,
    positions: List Position,
    wonTeam: Maybe Team
}

type alias PlayerMoveResult = {
    moveResults: MoveResults,
    canContinue: Bool
}
