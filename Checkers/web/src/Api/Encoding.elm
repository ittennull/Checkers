module Encoding exposing(
    encodeGetPossiblePositionsRequest,
    encodeMovePieceRequest,
    encodeBoard
    )

import Json.Encode as Encode exposing(..)
import Dto exposing (..)


encodeMovePieceRequest board pieceId position =
    object [
        ("board", encodeBoard board),
        ("pieceId", int pieceId),
        ("position", encodePosition position)
    ]

encodeGetPossiblePositionsRequest board pieceId onlyJump =
    object [
        ("board", encodeBoard board),
        ("pieceId", int pieceId),
        ("onlyJump", bool onlyJump)
    ]

encodeBoard board =
    let encodedPieces = List.map encodePiece board.pieces
    in  object [
            ("pieces", list encodedPieces)
        ]

encodePiece piece =
    let pieceDto = piece.pieceDto
    in  object [
            ("id", int pieceDto.id),
            ("position", encodePosition pieceDto.position),
            ("crowned", bool pieceDto.crowned),
            ("team", encodeTeam pieceDto.team)
        ]

encodePosition position =
    object [
        ("x", int position.row),
        ("y", int position.column)
    ]

encodeTeam team =
    let str = if team == White then "White" else "Black"
    in  string str |> encodeOption

encodeOption value =
    object [
        ("case", value)
    ]