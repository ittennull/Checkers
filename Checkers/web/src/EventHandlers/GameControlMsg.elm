module GameControlMsg exposing(
    GameControlMsg(..)
    )

import Http exposing (Error)
import Dto exposing (..)
import Types exposing (..)


type GameControlMsg 
    = PieceClick Piece
    | PlaceClick Position
    | FinishMove
    -- api responses
    | GetPossiblePositionsResponse (Result Http.Error (List Position))
    | MovePieceResponse (Result Http.Error PlayerMoveResult)
    | AIMovePieceResponse (Result Http.Error MoveResults)