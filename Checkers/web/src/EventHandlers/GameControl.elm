module GameControl exposing(
    gameControlUpdate
    )

import GameControlMsg exposing (..)
import Model exposing (..)
import Types exposing (..)
import Msg exposing (..)
import Dto exposing (..)
import Utils exposing (..)
import ApiClient as Api
import Animation exposing (..)
import Board exposing (..)
import Time exposing (second, millisecond)


gameControlUpdate :GameControlMsg -> Model -> (Model, Cmd Msg)
gameControlUpdate msg model =
    case msg of
        PieceClick piece -> selectPiece piece model
        PlaceClick position -> onPlaceClick position model
        GetPossiblePositionsResponse response -> logHttpErrorOrProceed model response onGetPossiblePositions
        MovePieceResponse response -> logHttpErrorOrProceed model response onMovePiece
        AIMovePieceResponse response -> logHttpErrorOrProceed model response onAIMovePiece
        FinishMove ->  onFinishMove model

createMoveAnimations: Board -> MoveResults -> List MoveAnimation
createMoveAnimations board moveResults =
    let createMoveAnimation movingPieceId fromPosition toPosition =
        let createAnimation fromCoordinate toCoordinate = 
                animation 0 |> from fromCoordinate |> to toCoordinate |> duration (500*millisecond)

            fromCoordinates = transformPositionToCoordinates fromPosition
            toCoordinates = transformPositionToCoordinates toPosition

            shouldBeCrowned = 
                moveResults.crownedPosition 
                |> Maybe.map ((==) toPosition)
                |> Maybe.withDefault False

        in  {
                time = 0,
                movingPieceId = movingPieceId,
                targetPosition = toPosition,
                xAnimation = createAnimation fromCoordinates.x toCoordinates.x,
                yAnimation = createAnimation fromCoordinates.y toCoordinates.y,
                crownedPosition = if shouldBeCrowned then moveResults.crownedPosition else Nothing
            }

        createAnimations movingPieceId =
            let createPairs list =
                case list of
                    x1::x2::xs -> (x1, x2) :: createPairs (x2::xs)
                    _ -> []

            in  createPairs moveResults.positions
                |> List.map (\(from, to) -> createMoveAnimation movingPieceId from to)

    in  createAnimations moveResults.pieceId 

findPieceById id pieces =
    pieces 
    |> List.filter (\x -> x.pieceDto.id == id)
    |> List.head

createDisappearAnimations board moveResults =
    let createAnimation (index, piece) = 
        {
            time = 0,
            piece = piece,
            animation = animation 0 |> delay ( (-250 + 500 * (index + 1)) * millisecond) |> from 1 |> to 0 |> duration (500*millisecond)
        }

        eatenPiecesCount = List.length moveResults.eatenPieceIds
        indices = List.range 0 (eatenPiecesCount - 1) |> List.map toFloat

    in  moveResults.eatenPieceIds 
        |> List.filterMap (\id -> findPieceById id board.pieces)
        |> List.map2 (,) indices
        |> List.map createAnimation

onMovePiece: Model -> PlayerMoveResult -> (Model, Cmd Msg)
onMovePiece model playerMoveResult =
    let finishAction (model, cmd) = 
            if playerMoveResult.canContinue then
                let newCmd = Api.getPossiblePositions model.board playerMoveResult.moveResults.pieceId True
                    newModel = {model | playerHasNotFinishedMove = True}
                in  (newModel, newCmd)
            else
                let newCmd = Cmd.batch [cmd, Api.aiMovePiece model.board]
                    oldBoard = model.board
                    newModel = {model | 
                                    playerHasNotFinishedMove = False,
                                    board = {oldBoard | selectedPieceId = Nothing}}
                in  (newModel, newCmd)
                
    in  movePiece model playerMoveResult.moveResults finishAction

onAIMovePiece model moveResults =
    movePiece model moveResults identity

movePiece model moveResults finishAction =
    let moveAnimations = createMoveAnimations model.board moveResults
        disappearAnimations = createDisappearAnimations model.board moveResults
        oldBoard = model.board
        onFinish (model, cmd) = 
            let newBoard = createNewBoard moveResults.board oldBoard.selectedPieceId
                gameState = 
                    moveResults.wonTeam
                    |> Maybe.map (\team -> if team == White then Won else Lost)
                    |> Maybe.withDefault Types.InProgress
                newModel = {model | board = newBoard, gameState = gameState}
            in  finishAction (newModel, cmd)

    in  ({model | 
            moveAnimations = moveAnimations,
            pieceDisappearAnimations = disappearAnimations,
            animationFinishAction = Just (AnimationFinishAction onFinish),
            board = {oldBoard | highlightedPlaces = []}
            }, Cmd.none)

onFinishMove model =
    let oldBoard = model.board
        newModel = {model | 
                        playerHasNotFinishedMove = False,
                        board = {oldBoard | selectedPieceId = Nothing}}
    in  (newModel, Api.aiMovePiece model.board)

onGetPossiblePositions model positions =
    let oldBoard = model.board
        board = {oldBoard | highlightedPlaces = positions}
    in  ({model | board = board}, Cmd.none)

onPlaceClick position model =
    model.board.selectedPieceId
    |> Maybe.map (\selectedPieceId -> (model, Api.movePiece model.board selectedPieceId position))
    |> Maybe.withDefault (model, Cmd.none)

selectPiece: Piece -> Model -> (Model, Cmd Msg)
selectPiece piece model =
    let 
        oldBoard = model.board
        newBoard = {oldBoard | selectedPieceId = Just piece.pieceDto.id}
    in ({model | board = newBoard}, Api.getPossiblePositions model.board piece.pieceDto.id False)
