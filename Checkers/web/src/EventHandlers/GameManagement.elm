module GameManagement exposing (
    gameManagementUpdate
    )

import ApiClient as Api
import Utils exposing (..)
import Types exposing (..)
import Encoding exposing (..)
import Decoding exposing (..)
import Board exposing (..)
import Ports exposing (..)
import Msg exposing (..)
import Model exposing (..)
import Json.Decode as Decode exposing(decodeValue)
import GameManagementMsg exposing (..)


gameManagementUpdate : GameManagementMsg -> Model -> (Model, Cmd Msg)
gameManagementUpdate msg model =
    case msg of
        NewGameApiRequest -> onNewGameApiRequest model
        NewGameApiResponse response -> logHttpErrorOrProceed model response onNewGameApiResponse
        LoadGame -> onLoadGame model
        GameLoaded boardDtoValue -> onGameLoaded model boardDtoValue
        SaveGame -> onSaveGame model

onNewGameApiRequest model = (model, Api.startNewGame)

onNewGameApiResponse model boardDto =
    let board = createNewBoard boardDto Nothing
    in  ({model | 
            board = board,
            gameState = Types.InProgress,
            playerHasNotFinishedMove = False
            }, 
            Cmd.none)

onLoadGame model =
    (model, loadGame ())

onGameLoaded: Model -> Decode.Value -> (Model, Cmd Msg)
onGameLoaded model boardDtoValue =
    case decodeValue decodeBoard boardDtoValue of
        Ok boardDto -> 
            let newBoard = createNewBoard boardDto Nothing
                newModel = {model | 
                                board = newBoard,
                                gameState = Types.InProgress,
                                playerHasNotFinishedMove = False}
            in  (newModel, Cmd.none)
        Err error -> Debug.log error (model, Cmd.none)

onSaveGame model =
    let boardValue = encodeBoard model.board
    in  (model, saveGame boardValue)