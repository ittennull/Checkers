module AnimationHandler exposing(
    onAnimationFrameTick,
    isAnimationInProgress
    )

import Types exposing (..)
import Model exposing (..)
import Animation exposing (..)
import Msg exposing (..)
import Time exposing (second, millisecond)

type AnimationState = InProgress | Finished Time.Time

onAnimationFrameTick: Time.Time -> Model -> (Model, Cmd Msg)
onAnimationFrameTick time model =
    let (newModel1, cmd1) = pieceMoveAnimationTick time model
        (newModel2, cmd2) = pieceDisappearAnimationTick time newModel1
        cmds = Cmd.batch [cmd1, cmd2]
    in  (newModel2, cmds)

isAnimationInProgress model =
    not (List.isEmpty model.moveAnimations) || not (List.isEmpty model.pieceDisappearAnimations)

performAnimationFinishAction: Model -> Maybe (Model, Cmd Msg)
performAnimationFinishAction model =
    model.animationFinishAction
    |> Maybe.map (\(AnimationFinishAction action) -> 
        if model.moveAnimations == [] && model.pieceDisappearAnimations == [] then
            let (newModel, cmd) = action (model, Cmd.none)
            in  ({newModel | animationFinishAction = Nothing}, cmd)
        else
            (model, Cmd.none)
        )

pieceMoveAnimationTick time model = 
    let animatePiece moveAnimation =
            let newTime = moveAnimation.time + time
                newTimeMilliseconds = newTime * millisecond
                newCoord = {
                    x = animate newTimeMilliseconds moveAnimation.xAnimation,
                    y = animate newTimeMilliseconds moveAnimation.yAnimation
                }
                newMoveAnimation = {moveAnimation | time = newTime}
                animationState = 
                    if  Animation.isDone newTimeMilliseconds moveAnimation.xAnimation then 
                        let timeRest = newTimeMilliseconds - (Animation.getDuration moveAnimation.xAnimation)
                        in  Finished timeRest
                    else
                        InProgress
            in  (newCoord, newMoveAnimation, animationState)

        updatePositionAndCoordinates piece newCoord movingAnimation animationState =
            let newPosition =
                    case animationState of
                        Finished _ -> movingAnimation.targetPosition
                        InProgress -> piece.pieceDto.position
                oldPieceDto = piece.pieceDto
            in  {piece | 
                    coordinates = newCoord, 
                    pieceDto = {oldPieceDto | 
                                position = newPosition}}

        updateCrown piece crownedPositionOption =
            case crownedPositionOption of
                Just crownedPosition ->
                    if piece.pieceDto.position == crownedPosition then  
                        let oldPieceDto = piece.pieceDto
                        in  { piece | pieceDto = {oldPieceDto | crowned = True}}
                    else
                        piece
                _ -> piece

        playAnimation moveAnimation =
            let (newCoord, newMoveAnimation, animationState) = animatePiece moveAnimation
                (movingPieceList, otherPieces) = model.board.pieces |> List.partition (\x -> x.pieceDto.id == newMoveAnimation.movingPieceId)
                newBoard = 
                    movingPieceList
                    |> List.head
                    |> Maybe.map (\movingPiece ->
                        let newMovingPiece1 = updatePositionAndCoordinates movingPiece newCoord newMoveAnimation animationState
                            newMovingPiece2 = updateCrown newMovingPiece1 newMoveAnimation.crownedPosition
                            oldBoard = model.board
                        in  {oldBoard | pieces = newMovingPiece2 :: otherPieces}
                        )
                    |> Maybe.withDefault model.board
            in  (newBoard, newMoveAnimation, animationState)

    in  case model.moveAnimations of
            moveAnimation::restOfMoveAnimations ->
                let (newBoard, newMoveAnimation, animationState) = playAnimation moveAnimation
                    newModel = {model | board = newBoard, moveAnimations = restOfMoveAnimations}
                in  case animationState of
                        InProgress ->
                            let newModel2 = {newModel | moveAnimations = newMoveAnimation :: restOfMoveAnimations}
                            in  (newModel2, Cmd.none)
                        Finished timeRest -> 
                            let newModel2 = {newModel | moveAnimations = restOfMoveAnimations}
                            in  pieceMoveAnimationTick timeRest newModel2

            [] -> performAnimationFinishAction model |> Maybe.withDefault (model, Cmd.none)

pieceDisappearAnimationTick: Time.Time -> Model -> (Model, Cmd Msg)
pieceDisappearAnimationTick time model =
    let playAnimation pieceDisappearAnimation board =
        let newTime = pieceDisappearAnimation.time + time
            newTimeMilliseconds = newTime * millisecond
            newOpacity = animate newTimeMilliseconds pieceDisappearAnimation.animation
            isDone = Animation.isDone newTimeMilliseconds pieceDisappearAnimation.animation
            otherPieces = board.pieces |> List.filter (\x -> x.pieceDto.id /= pieceDisappearAnimation.piece.pieceDto.id)
            oldPiece = pieceDisappearAnimation.piece
            newPiece = {oldPiece | opacity = newOpacity}
            newBoard = {board | pieces = newPiece :: otherPieces}
            newPieceDisappearAnimation = {pieceDisappearAnimation | piece = newPiece, time = newTime}
        in  (newBoard, newPieceDisappearAnimation, isDone)

        reduce pieceDisappearAnimation (activeAnimations, board) = 
            let (newBoard, newPieceDisappearAnimation, isDone) = playAnimation pieceDisappearAnimation board
                animations = if isDone then activeAnimations else newPieceDisappearAnimation::activeAnimations
            in  (animations, newBoard)

        (activeAnimations, newBoard) = List.foldr reduce ([], model.board) model.pieceDisappearAnimations

        newModel = {model | 
                        board = newBoard,
                        pieceDisappearAnimations = activeAnimations}

    in  performAnimationFinishAction newModel |> Maybe.withDefault (newModel, Cmd.none)