module GameManagementMsg exposing(
    GameManagementMsg (..)
    )

import Json.Decode as Decode
import Dto exposing (..)
import Http exposing(Error)

type GameManagementMsg
    = NewGameApiRequest
    | NewGameApiResponse (Result Http.Error BoardDto)
    | LoadGame
    | GameLoaded Decode.Value 
    | SaveGame