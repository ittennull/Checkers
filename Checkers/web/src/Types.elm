module Types exposing(..)

import Dto exposing (..)
import Animation exposing (..)
import Time


type alias Coordinates = {
    x: Float,
    y: Float
}

type alias Piece = {
    pieceDto: PieceDto,
    coordinates: Coordinates,
    opacity: Float
}

type alias Board = {
    pieces: List Piece,
    selectedPieceId: Maybe Int,
    highlightedPlaces: List Position
}

type alias MoveAnimation = {
    time: Time.Time,
    movingPieceId: Int,
    targetPosition: Position,
    xAnimation: Animation,
    yAnimation: Animation,
    crownedPosition: Maybe Position
}

type alias OpacityAnimation = {
    time: Time.Time,
    piece: Piece,
    animation: Animation
}

type GameState = Won | Lost | InProgress